#!/bin/sh

# TODO: add build-depends & build-depends-indep

grep-dctrl -n -s Package -w -F Pre-Depends,Depends,Recommends "python3-six" --or "python3-future" /var/lib/apt/lists/*_debian_dists_testing_*_binary-amd64_Packages | sort -u > testing
grep-dctrl -n -s Package -w -F Pre-Depends,Depends,Recommends "python3-six" --or "python3-future" /var/lib/apt/lists/*_debian_dists_unstable_*_binary-amd64_Packages | sort -u > sid
diff -u testing sid | grep -e ^+ -e ^-
