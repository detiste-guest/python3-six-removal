#!/bin/sh

# search for more python3 crumbs

dpkg -S $(grep 'from __future__ import print_function' /usr/lib/python3/dist-packages/ -rl) 2>/dev/null | cut -d: -f1 | sort -u
