#!/bin/bash
set -e
set -u

package=$(cat liste | grep -v '#' | grep -v 'http' | sort -R | head -n 1 | awk '{print $1}')
echo package: "$package"

# quick & dirty: try again
if grep -q $package pr
then
    exec ./random.sh
fi

google-chrome "https://tracker.debian.org/pkg/$package"

homepage=$(apt show "$package" 2> /dev/null | grep ^Home | awk '{print $2}')
echo homepage: "$homepage"
google-chrome "$homepage"


