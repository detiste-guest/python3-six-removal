#!/usr/bin/python3

# https://udd.debian.org/schema/udd.html#public.table.popcon-src

# https://udd.debian.org/schema/udd.html#public.table.upstream

import os

import apt_pkg

apt_pkg.init()
SOURCES = apt_pkg.SourceRecords()

def read(dir):
    sel: set[str] = set()
    with open(os.path.join(dir, 'liste'), 'r') as fd:
         for line in fd:
              package = line.split()[0]
              sel.add(package)
    return sel

mock = read('mock')
nose = read('nose')
six = read('six')
pkgs = mock | nose | six

def skip() -> dict[str, str]:
    versions: dict[str, str] = dict()
    with open('skip', 'r') as fd:
        for line in fd:
            line = line.strip('\n')
            source, version = line.split()
            versions[source] = version
    return versions

to_skip = skip()

import psycopg2
conn = psycopg2.connect("postgresql://udd-mirror:udd-mirror@udd-mirror.debian.net/udd")
cursor = conn.cursor()
cursor.execute("""
    SELECT u.source, max(upstream_version) as upstream_version
    FROM upstream u
    INNER JOIN sources s
    ON u.source = s.source
    WHERE u.status = 'newer package available'
    AND s.release = 'sid'
    AND (s.maintainer_email like '%python%' or s.maintainer_email like '%qa.debian.org%' or s.maintainer_email like '%debian-med-packaging%' or s.maintainer_email like '%science%')
    GROUP BY u.source
""")
upgrades = set()
for row in cursor.fetchall():
    source, version = row
    if to_skip.get(source) == version:
        continue
    if SOURCES.lookup(source):
        for binary in SOURCES.binaries:
            upgrades.add(binary)

todos = pkgs & upgrades
for todo in sorted(todos):
    m = 'm' if todo in mock else ' '
    n = 'n' if todo in nose else ' '
    s = 's' if todo in six else ' '
    print("%-30s" % todo, m, n, s)
