#!/usr/bin/python3

import datetime
import os

HEADLESS = 'DISPLAY' not in os.environ

if HEADLESS:
    import matplotlib as mpl
    mpl.use('Agg')

import matplotlib.pyplot as plt
import matplotlib.dates as mdates
import matplotlib.ticker as mtick

from git import Repo

def graph(package):
    repo = Repo.init('/home/tchet/%s' % package)
    count = dict()

    for c in repo.iter_commits(all=True, paths=['liste']):
        blob = c.tree['liste']
        txt = blob.data_stream.read().decode()
        count[c.authored_date] = len(txt.splitlines())

    stamps = []
    todo = []
    for k, v in sorted(count.items()):
        stamps.append(datetime.datetime.fromtimestamp(k))
        todo.append(v)
    return stamps, todo

st_six, c_six = graph('six')
st_mock, c_mock = graph('mock')
st_pkg, c_pkg = graph('pkg_resources')

years = mdates.YearLocator()   # every year
months = mdates.MonthLocator()  # every month
yearsFmt = mdates.DateFormatter('%Y')

fig, ax = plt.subplots()
ax.grid(True)
ax.plot(st_six, c_six, label='six')
ax.plot(st_mock, c_mock, label='mock')
ax.plot(st_pkg, c_pkg, label='pkg_resources')

datemin = min(st_six)                                 - datetime.timedelta(days=1)
datemax = max(max(st_six), max(st_mock), max(st_pkg)) + datetime.timedelta(days=1)
#datemax = max(datemax, datetime.datetime(2024,4,20))
ax.set_xlim(datemin, datemax)

if not HEADLESS:
    #message box GUI
    ax.format_xdata = mdates.DateFormatter('%Y-%m-%d')

ax.set_ylim(0, 600)

# https://matplotlib.org/stable/gallery/text_labels_and_annotations/annotate_transform.html
bbox = dict(boxstyle="round", fc="0.8")
arrowprops = dict(
    arrowstyle="->",
    connectionstyle="angle,angleA=0,angleB=90,rad=10")

def annotate(label, ts, y):
    ax.annotate(label,
                xy=(ts, y),
                xytext=(ts + datetime.timedelta(days=12), y-100),
                bbox=bbox, arrowprops=arrowprops)

annotate('add sid', datetime.date(2023,7,22), 530)
# https://wiki.debian.org/DebianEvents/gb/2023/MiniDebConfCambridge
# Mini-debconf in Cambridge, UK. Thursday 23rd to Sunday 26th November 2023
annotate('add future', datetime.date(2023,11,23), 494)
annotate('xz', datetime.date(2024,3,30), 360)

fig.autofmt_xdate()

plt.legend()
#plt.savefig('/var/www/html/six.png' % package)

if not HEADLESS:
    plt.show()
