#!/usr/bin/python3

# wget http://popcon.debian.org/by_inst.gz --no-clobber

# example Django PR with metaclass:
#   https://github.com/bennylope/django-organizations/pull/255
#   https://github.com/bennylope/django-organizations/commit/571b51ed9c16fcb0b7fa754460308fa53d9be00f

import gzip

import apt
import apt_pkg
import debianbts as bts
from aptsources.sourceslist import SourcesList

apt_pkg.init()
SOURCES = apt_pkg.SourceRecords()
CACHE = apt.Cache()

def get_popcon() -> dict[str, int]:
    popcon: dict[str, int] = dict()
    with gzip.open('by_inst.gz', 'rb') as f:
        file_content = f.read().decode('latin1')

        for line in file_content.split('\n'):
            if not line or line[0] in ('#','-'):
               continue
            package, score = line.split()[1:3]
            popcon[package] = int(score)
    return popcon

popcon = get_popcon()

def get_todo() -> dict[str, int]:
    todo: dict[str, int] = dict()
    with open('testing', 'r') as f:
        for package in f:
            package = package.strip('\n')
            todo[package] = popcon.get(package, 0)
    with open('sid', 'r') as f:
        for package in f:
            package = package.strip('\n')
            todo[package] = popcon.get(package, 0)
    return todo

todo = get_todo()

def get_patch() -> dict[str, str]:
    '''patch sent/existing upstream'''
    patch: dict[str, str] = dict()
    with open('pr', 'r') as fd:
        for line in fd:
            package, url = line.strip('\n').split(None, 1)
            patch[package] = url
    return patch

patch = get_patch()

def get_pending() -> dict[str, int]:
    pending: dict[str, int] = dict()
    bugs = bts.get_usertag('python3-six@packages.debian.org', tags=['python3-six-removal'])['python3-six-removal']
    for bug in bts.get_status(bugs):
        package = bug.package.split(':')[-1]
        if package == 'ftp.debian.org':
            if 'RM:' in bug.subject:
                try:
                    package = bug.affects[0].split(':')[-1]
                except IndexError:
                    package = bug.subject.split(':')[1].strip(' ').split()[0]
        pending[package] = bug.bug_num
    return pending

pending = get_pending()

# bug may have been filled against the source package
# -> move it to all the python3-#### binaries
for source in sorted(pending.keys()):
    if source in CACHE:
       # this is a binary package
       continue
    if SOURCES.lookup(source):
       for binary in SOURCES.binaries:
           if binary not in pending and binary.startswith('python3'):
               pending[binary] = pending[source]


def get_section(package: str) -> str:
    try:
        p = CACHE[package]
    except KeyError:
        return '?'
    if p.candidate is None:
        return '?'
    return p.candidate.section


for k, v in sorted(todo.items(), key=lambda item: (item[1],item[0])):
    line = '%-40s %-6s' % (k, v)
    section = get_section(k)
    if k in pending:
        line += ' #%s' % pending[k]
    elif k in patch:
        line += ' (%s)' % patch[k]
    elif 'old' in section:
        line += ' (%s)' % section
    print(line)
